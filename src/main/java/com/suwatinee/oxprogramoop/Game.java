/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suwatinee.oxprogramoop;

import java.util.Scanner;

/**
 *
 * @author suwat
 */
public class Game {

    Scanner kb = new Scanner(System.in);
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void ShowWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void ShowTable() {
        table.ShowTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error table at row and col is not Empty!!!");
        }
    }

    public void ShowTurn() {
        System.out.println(table.getcurrentPlayer().getName() + " turn");
    }

    public void run() {
        this.ShowWelcome();
        while (true) {
            this.ShowTable();
            this.ShowTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
                    System.out.println("Bye Bye.");
                } else {
                    System.out.println(table.getWinner().getName() + " Win!");
                    System.out.println("Bye Bye.");
                }
                break;

            }
            table.switchPlayer();
        }

    }
}
