/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suwatinee.oxprogramoop;

/**
 *
 * @author suwat
 */
public class Table {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currentPlayer = X;

    }

    public void ShowTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            return true;
        }
        return false;

    }

    public Player getcurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
       
    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void checkX() {
            if (table[0][0] == table[1][1]
                && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            finish = true;
            winner = currentPlayer;
        }
        return;
    }  

    public void checkDraw() {
        playerO.draw();
        playerX.draw();
    }

    public void checkWin() {
        checkCol();
        checkRow();
        checkX();       
        checkDraw();
    }

    public boolean isFinish() {
        return finish;
    }
    

    public Player getWinner() {
        return winner;
    }
}
